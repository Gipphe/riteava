module.exports = {
	extends: './node_modules/sanctuary-style/eslint-es6.json',
	parser: 'babel-eslint',
	parserOptions: {
		sourceType: 'module',
	},
	env: {
		es6: true,
		node: true,
	},
	rules: {
		'no-tabs': 'off',
	},
};
