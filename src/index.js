import deepEqual from 'deep-equal';
import Future from 'fluture';
import S from 'sanctuary';
import {def} from './hm';

// Assert ::
//  { given :: string
//  , should :: string
//  , actual :: a
//  , expected :: a
//  } -> ()

// testsToRun :: Map String { skip :: boolean, tests :: (List TestRecord) }
const testsToRun = {};
const addTest = def
	('addTest :: String -> TestRecord -> ()')
	(unit => test => {
		if (testsToRun[unit] === undefined) {
			testsToRun[unit] = [];
		}
		testsToRun[unit].push (test);
	});

const equal = actual => expected =>
	deepEqual (actual, expected, {strict: true});


const makeDescription = def
	('makeDescription :: String -> String -> String')
	(given => should =>
		`Given ${given}: should ${should}`);

const skip = def
	('skip :: String -> AssertionRecord -> ()')
	(unit => ({given, should, actual, expected}) => {
		const description = makeDescription (given) (should);
		addTest (unit) ({description, fn: S.Nothing});
	});

const test = def
	('test :: String -> (AssertFunction -> ()) -> ()')
	(unit => contextFn => {
		console.log (`# ${unit}`);

		const assert = def
			('assert :: AssertionRecord -> ()')
			(({given, should, actual, expected}) => {
				const description = makeDescription (given) (should);
				addTest ({
					description,
					fn: S.Just (() => equal (actual) (expected)),
				});
			});
		contextFn (assert);
	});

const isPromise = def
	('isPromise :: Any -> Boolean')
	(x => x && typeof x.then === 'function');

const tallyTests = () => {
	if (testsToRun.length === 0) {
		console.log ('No tests found');
		return;
	}

	console.log ('TAP version 13');
	console.log ();

	const awaitTestFn =
		('awaitTestFn :: TestRecord -> ()')
		(({description, fn}) =>
			S.maybe
				(Future.of ({description, res: S.Nothing}))
				(S.pipe ([
					({description, fn}) =>
					f => {
						const res = f ();
						if (Future.isFuture (res)) {
							return res;
						} else if (isPromise (res)) {
							return Future.tryP (() => res);
						}
						return Future.of (res);
					},
					S.map (x => ({description, res: S.Just (x)})),
					Future.fold (S.lefts, S.right),
				]))
				(fn));

	const promises = S.map
		(({description, fn}) =>
			S.maybe
				(Promise.resolve ({description, res: S.Nothing}))
				(f => Promise.resolve (f ())
					.then (res => ({description, res: S.Just (res)})))
				(fn))
		(testsToRun);

	console.log (`1..${totalCount ()}`);
	console.log (`# tests ${totalCount ()}`);
	if (passCount > 0) {
		console.log (`# pass  ${passCount}`);
	}
	if (failCount > 0) {
		console.log (`# fail  ${failCount}`);
		console.log ('\n# not ok');
	} else {
		console.log ('\n# ok');
	}
};

process.on ('beforeExit', tallyTests);

test.skip = skip;

export default test;
export {test, skip};
