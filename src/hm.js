import $ from 'sanctuary-def';
import {create} from 'hm-def';
import {env} from './types';

export const def = create ({
	$,
	env: $.env
		.concat (env),
	checkTypes: Boolean (process.env.SANCTUARY_CHECK_TYPES_RITETEST),
	typeClasses: [],
});
