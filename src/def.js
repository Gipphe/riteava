import {env} from 'sanctuary';
import {create} from 'sanctuary-def';

export const def = create ({
	checkTypes: Boolean (process.env.SANCTUARY_CHECK_TYPES_RITETEST),
	env,
});
