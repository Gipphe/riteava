import $ from 'sanctuary-def';

const a = $.TypeVariable ('a');

export const TestRecord = $.RecordType ({
	description: $.String,
	fn: $.Maybe ($.Thunk ($.Boolean)),
});

export const AssertionRecord = $.RecordType ({
	given: $.String,
	should: $.String,
	actual: a,
	expected: a,
});

export const AssertFunction = $.Function ([AssertionRecord, $.Undefined]);

export const env = [
	TestRecord,
	AssertionRecord,
	AssertFunction,
];
